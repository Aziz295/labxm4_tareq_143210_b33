<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Upload Your picture</h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Click Here</button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Please Insert this field</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-body">
                    <label for="usr">Descriptions:</label>
                    <input type="text" class="form-control" id="usr" placeholder="About Image">
                </div>
                <div class="modal-body">
                    <label for="current time">Pick up date</label>
                    <input type="date" class="form-control" id="current time">
                </div>
                <div class="form-group" action="display.php" method="post" enctype="multipart/form-data" >
                    <label class="control-label">upload file</label>
                    <input type="file" name="fileToupload" id="fileToupload" class="filestyle" data-icon="false">
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="location.href='display.php'" class="btn btn-default" data-dismiss="modal" name="submit">Send</button>
                </div>
            </div>

        </div>
    </div>

</div>

</body>
</html>
