<?php
$date=$_POST['date'];
$description=$_POST['description'];
$submit=$_POST['submit'];
$filename = $_FILES['fileToUpload']['tmp_name'];

if(isset($submit)) {
    if (isset($date) && isset($description) && isset($filename))
    {
        $located_folder = 'received_files/';
        $filename = $_FILES['fileToUpload']['tmp_name'];
        $uploaded_file = time() . $_FILES['fileToUpload']['name'];
        $destination = $located_folder . $uploaded_file;
        move_uploaded_file($filename, $destination);
    }

}




?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
    </style>
</head>
<body>

<div class="container">
    <br>
    <div id="myCarousel" class="carousel slide" >
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox"  >

            <div class="item active" data-ride="carousel">
                <img src="<?php echo $destination;?>" alt="Chania" width="460" height="345">
                <div class="carousel-caption" >
                    <?php echo "</br>". "Description: ". $description."</br>";
                    echo "Date: ". $date."</br>";
                    ?>
                </div>
            </div>

            <div class="item" data-ride="carousel">
                <img src="<?php echo $destination;?>" alt="Chania" width="460" height="345">
                <div class="carousel-caption">
                    <?php echo "</br>". "Description: ". $description."</br>";
                    echo "Date: ". $date."</br>";
                    ?>
                </div>
            </div>
            <div class="item" data-ride="carousel">
                <img src="<?php echo $destination;?>" alt="Chania" width="460" height="345">
                <div class="carousel-caption">
                    <?php echo "</br>". "Description: ". $description."</br>";
                    echo "Date: ". $date."</br>";
                    ?>
                </div>
            </div>

        </div>


        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


</div>


</body>
</html>
